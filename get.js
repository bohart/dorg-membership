(function(countries) {
  var page_path = "https://assoc.drupal.org/membership/individuals?country=All&page=";
  var page_max = 6;
  var count = {};

  // Pager.
  for (var page = 0; page < page_max; page++) {
    jQuery.ajax({
      url: page_path + page,
      success: function(html) {
        console.log('Page loaded: ' + (page + 1));

        // Countries.
        for (var i = 0; i < countries.length; i++) {
          var country = countries[i];
          count[country] = count[country] || 0;
          count[country] += jQuery("td[data-th=Country]:contains('" + country + "')", html).length;

          if (page == (page_max - 1)) {
            console.log('Country: ' + country + '. Count = ' + count[country]);
          }
        }
      },
      async: false
    });
  }
})(["Ukraine", "Italy", "France"]);